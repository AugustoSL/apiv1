<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Register extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $code;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        //
        $this->user = $user;
        $this->code = mt_rand(1000, 9999);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	$this->user->activation_code = $this->code;
    	$this->user->save();
    	
        return $this->markdown('emails.register');
    }
}

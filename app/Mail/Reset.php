<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use JWTAuth;
use JWTFactory;
use Illuminate\Support\Facades\DB;

class Reset extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $code;
    public $url;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
    	//
    	$this->user = $user;
    	$this->code = str_random(100);
    	$this->url = "http://laravel-api.app/api/users/reset/password/{$user->email}/{$this->code}";
//     	$this->code = JWTAuth::fromUser($user);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//     	$this->user->reset_code = $this->code;
//     	$this->user->save();

    	// Delete
    	DB::table('password_resets')->where('email', $this->user->email)->delete();
    	
    	// Insert
    	DB::table('password_resets')->insert([
    			'email' => $this->user->email,
    			'token' => $this->code
    	]);
    	
        return $this->markdown('emails.reset');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Support\Facades\Mail;
use App\Mail\Register;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Mail\Activate;
use App\Mail\Reset;
use App\Mail\PasswordChange;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserController extends Controller {
	
	/**
	 * GET /
	 * 
	 * @return unknown
	 */
	public function index() {
		
		return response([ 
				'users' => User::all()
		], Response::HTTP_OK);
	}
	
	/**
	 * GET /{id}
	 * @param unknown $id
	 */
	public function show($id) {
		return response(['user'=> User::find($id)], Response::HTTP_FOUND);
	}
	
	/**
	 * Signup a user
	 * 
	 * @param Request $request
	 * @return json
	 */
	public function signup(Request $request) {
		$this->validate ( $request, [ 
			'name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|confirmed' 
		] );
		
		$user = User::create ( [ 
			'name' => $request->input ( 'name' ),
			'email' => $request->input ( 'email' ),
			'password' => bcrypt($request->input ( 'password' )) 
		] );
		
		$mailRegister = new Register($user);
		
		Mail::to($user)->send($mailRegister);
		
		return response()->json([
				'message' => 'User created, please check your email and use the sent code to activate your new account.'
		], Response::HTTP_CREATED);
	}
	
	/**
	 * Signin a user
	 * 
	 * @param Request $request
	 * @return token
	 */
	public function signin(Request $request) {
		$this->validate ( $request, [
				'email' => 'required|email',
				'password' => 'required'
		] );
		$credentials = $request->only('email', 'password');
				
		try {
			if(!$token = JWTAuth::attempt($credentials)) {
				return response()->json([
						'error' => 'invalid credentials'
				], Response::HTTP_UNAUTHORIZED);
			}			
		} catch(JWTException $e) {
			return response()->json([
					'error' => 'Could not create the token'
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
		
		// Checks if the user activate hes account
		if(!User::where('email', $request->input('email'))->first(['active'])->active) {
			return response()->json([
					'error' => 'User is not active, please check your email address to activate your new account.'
			], Response::HTTP_SERVICE_UNAVAILABLE);
		}
		
		return response()->json([
				'token' => $token
		], Response::HTTP_OK);
	}
	
	/**
	 * Activate a user
	 * 
	 * @param Request $request
	 * @return unknown
	 */
	public function activate(Request $request) {
		$this->validate($request, [
				'email' => 'required|email',
				'code' => 'required|integer|min:1000|max:9999'
		]);
	
		try {
			$user = User::where('email', $request->input('email'))->firstOrFail();
		} catch(ModelNotFoundException $e) {
			return response()->json([
					'error' => 'The submitted email was not found'
			], Response::HTTP_NOT_FOUND);
		}
		
		if($user->active) {
			return response()->json([
					'message' => 'The account is active'
			], Response::HTTP_FOUND);
		}
		
		if($user->activation_code == $request->input('code')) {
			$user->activation_code = null;
			$user->active = true;
			$user->save();
			
			$activateMail = new Activate($user);
			
			Mail::to($user->email)->send($activateMail);
			
		} else {
			return response()->json([
					'error' => 'The submitted code is incorrect'
			], Response::HTTP_NOT_MODIFIED);
		}
		
		return response()->json([
				'message' => 'Your account was succefully activated, now you can login, enjoy! :)'
		], Response::HTTP_ACCEPTED);
	}
	
	/**
	 * PUT /reset/{code}
	 * 
	 * @param Request $request
	 */
	public function resetMail(Request $request) {
		$this->validate($request, [
				'email' => 'required|email'
		]);
		
		$email = $request->input('email');
		
		try {
			$user = User::where('email', $email)->firstOrFail();
		} catch(ModelNotFoundException $e) {
			return response()->json([
					'error' => 'E-mail not found'
			], Response::HTTP_NOT_FOUND);
		}
		
		$resetMail = new Reset($user);
		
		Mail::to($email)->send($resetMail);
		
		return response()->json([
				'message' => "The reset link was sent to {$email}"
		], Response::HTTP_OK);
	}
	
	/**
	 * PUT /reset/{code}
	 *
	 * @param Request $request
	 */
	public function resetPassword(Request $request) {
		$this->validate($request, [
				'email' => 'required|email',
				'password' => 'required|confirmed'
		]);
		
		$email = $request->input('email');
		$code = $request->input('code');
		$password = $request->input('password');
		
		// Checks user
		try {
			$user = User::where('email', $email)->firstOrFail();
		} catch(ModelNotFoundException $e) {
			return response()->json([
					'error' => 'The user was not found'
			], Response::HTTP_NOT_FOUND);
		}
		
		// Get reset request
		$resetRequest = DB::table('password_resets')->where('email', $email)->first();
		
		// Checks if found
		if(is_null($resetRequest)) {
			return response()->json([
					'error' => 'Reset request not found'
			], Response::HTTP_NOT_FOUND);
		}
		// Checks token
		if($resetRequest->token != $code) {
			return response()->json([
					'error' => 'Reset token invalid'
			], Response::HTTP_UNAUTHORIZED);
		}
				
		// Checks if token was expiry (> 1 hour)
		$tokenCreation = new Carbon($resetRequest->created_at);
		
		if($tokenCreation->diffInHours(Carbon::now()) > 1) {
			return response()->json([
					'error' => 'Expired reset token. Must request a new one.'
			], Response::HTTP_UNAUTHORIZED);
		}
		
		// Update password
		$user->password = bcrypt($password);
		$user->save();

		// Remove reset token
		DB::table('password_resets')->where('email', $email)->delete();
		
		$passwordChangedMail = new PasswordChange($user);
		
		Mail::to($email)->send($passwordChangedMail);
		
		return response()->json([
				'message' => 'The password was succefully changed'
		], Response::HTTP_ACCEPTED);
				
	}
	
}

@component('mail::message')
# Estimado {{ $user->name}},

Su clave ha sido cambiada satisfactoriamente.

Atentamente,<br>
{{ config('app.name') }}
@endcomponent

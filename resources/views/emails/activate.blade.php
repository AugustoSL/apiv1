@component('mail::message')
# Felicidades!, su cuenta ha sido activada satisfactoriamente!.

@component('mail::button', ['url' => 'http://www.gomove.cl/'])
Visita GOMOVE
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

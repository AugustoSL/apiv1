@component('mail::message')
# Bienvenido {{ $user->name }} porfavor active su cuenta con el siguiente código: {{ $code }}

@component('mail::button', ['url' => 'http://www.gomove.cl/'])
Visita GOMOVE
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent

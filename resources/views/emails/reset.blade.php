@component('mail::message')
# Estimado {{ $user->name }}, hemos recibido una solicitud para reiniciar su clave.

Haga click en el siguiente botón:

@component('mail::button', ['url' => $url])
Reiniciar clave
@endcomponent

Si tiene problemas visitando el link intente copiar y pegar el siguiente link:
{{ $url }}

Si usted no ha realizado esta solicitud, puede ignorar este mensaje.<br>
{{ config('app.name') }}
@endcomponent

<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'users'], function () {
	Route::post('/signup', [
			'uses' => 'UserController@signup'
	]);
	
	Route::post('/signin', [
			'uses' => 'UserController@signin'
	]);
	
	Route::put('/activate', [
			'uses' => 'UserController@activate'
	]);
	
	Route::post('/reset', [
			'uses' => 'UserController@resetMail'
	]);
	
	Route::put('/reset/password', [
			'uses' => 'UserController@resetPassword'
	]);
	
	Route::group(['middleware' => 'auth.jwt'], function() {		
		Route::get('/', ['uses' => 'UserController@index']);
		Route::get('/{id}', ['uses' => 'UserController@show']);
		
	});
});